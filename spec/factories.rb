
FactoryGirl.define do
  factory :user do
    name     'David Rojo'
    email    'rojo@testers.net'
    password 'some password'
    password_confirmation 'some password'
  end
end
