require 'rails_helper'

RSpec.describe User, :type => :model do
  before do
    @user = User.new name: 'Example User', email: 'user@example.com',
      password: 'foobar', password_confirmation: 'foobar'
  end

  subject { @user }

  it { should respond_to :name                  }
  it { should respond_to :email                 }
  it { should respond_to :password_digest       }
  it { should respond_to :password              }
  it { should respond_to :password_confirmation }
  it { should respond_to :authenticate          }

  it { should be_valid }

  context 'when «name» is not present' do
    before { @user.name = ' '    }
    it     { should_not be_valid }
  end

  context 'when «name» is too long' do
    before { @user.name = 'a' * 51 }
    it     { should_not be_valid   }
  end

  context 'when «email» is not present' do
    before { @user.email = ''    }
    it     { should_not be_valid }
  end

  context 'when «email» format is valid' do
    addresses = %w(user@foo.COM A_US-ER@f.b.org frst.lst@foo.jp a+b@baz.cn)
    addresses.each do |address|
      before { @user.email = address }
      it     { should be_valid       }
    end
  end

  context 'when «email» has a mixed case' do
    let(:mixed_case_address) { 'A_US-ER@f.b.org' }

    it 'downcases the address before saving' do
      @user.email = mixed_case_address
      @user.save

      expect(@user.email).to eql mixed_case_address.downcase
    end
  end

  context 'when «email» format is invalid' do
    addresses = %w(user@foo,com user_at_foo.org example@foo@bar.com
      foo@bar+baz.com foo@bar..com)

    addresses.each do |invalid_address|
      before { @user.email = invalid_address }
      it     { should_not be_valid           }
    end
  end

  context 'when «email» is already taken' do
    before do
      same_email_user       = @user.dup
      same_email_user.email = @user.email.downcase
      same_email_user.save
    end

    it { should_not be_valid }
  end

  context 'when «password» is not present' do
    before do
      @user = User.new name: 'Example User', email: 'user@example.com',
        password: '', password_confirmation: ''
    end

    it { should_not be_valid }
  end

  context 'when «password» does not match confirmation' do
    before { @user.password_confirmation = 'mismatch' }
    it     { should_not be_valid                      }
  end

  describe 'return value of authentication method' do
    before { @user.save }
    let(:found_user) { User.find_by email: @user.email }

    context 'with valid password' do
      it { should eq found_user.authenticate(@user.password) }
    end

    context 'with invalid password' do
      let(:invalid_pass_user) { found_user.authenticate('invalid') }

      it      { should_not eq invalid_pass_user        }
      specify { expect(invalid_pass_user).to be_falsey }
    end
  end

  context 'with a password that is too short' do
    before { @user.password = @user.password_confirmation = 'a' * 5 }
    it     { should be_invalid                                      }
  end
end
