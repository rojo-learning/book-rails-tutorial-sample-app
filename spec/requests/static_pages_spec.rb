require 'rails_helper'
include Rails.application.routes.url_helpers

RSpec.shared_examples 'a static page' do |path, options|
  before { visit path }

  it { expect(page).to have_title(full_title(options[:title])) }
  it { expect(page).to have_selector('h1', text: options[:h1])     }
end

RSpec.describe "Static Pages", :type => :request do
  subject { page }

  it 'links on the layout are right' do
    visit root_path

    click_link 'About'
    expect(page).to have_title(full_title('About Us'))
    click_link 'Help'
    expect(page).to have_title(full_title('Help'))
    click_link 'Contact'
    expect(page).to have_title(full_title('Contact'))
    click_link 'Home'
    click_link 'Sign up now!'
    expect(page).to have_title(full_title('Sign up'))
    click_link 'sample app'
    expect(page).to have_title(full_title(''))
  end

  describe "Home page" do
    it_behaves_like 'a static page', root_path, title: '', h1: 'Sample App'
    it { expect(page).not_to have_title('| Home') }
  end

  describe "Help page" do
    it_behaves_like 'a static page', help_path, title: 'Help', h1: 'Help'
  end

  describe "About page" do
    it_behaves_like 'a static page', about_path, title: 'About Us', h1: 'About Us'
  end

  describe "Contact page" do
    it_behaves_like 'a static page', contact_path, title: 'Contact', h1: 'Contact Information'
  end
end
