require 'rails_helper'

RSpec.describe "User pages", :type => :request do
  subject { page }

  describe "Sign Up page" do
    before { visit signup_path }

    it { should have_selector('//h1', text: 'Sign up') }
    it { should have_title(full_title('Sign up'))      }

    describe 'sign-up' do
      let(:submit) { "Create my account" }

      describe "with invalid information" do
        it "does not create the user" do
          expect { click_button submit }.not_to change(User, :count)
        end

        describe "after submission" do
          before { click_button submit }

          it { should have_title('Sign up') }
          it { should have_content('error') }
        end

        describe "sign up errors" do
          def displays_error(message, field:, data:)
            fill_in field, with: data
            click_button submit

            expect(subject).to have_content(message)
          end

          describe "on empty name" do
            let(:message) { "Name can't be blank"                }
            it { displays_error message, field: 'Name', data: '' }
          end

          describe "on empty email" do
            let(:message) { "Email can't be blank"                }
            it { displays_error message, field: 'Email', data: '' }
          end

          describe "on invalid email" do
            let(:message) { "Email is invalid"                    }
            it { displays_error message, field: 'Email', data: '' }
          end

          describe "on duplicated email" do
            let(:user) do
              User.create name: 'Example User',
                email: 'user@example.com',
                password: 'foobar',
                password_confirmation: 'foobar'
            end

            let(:message) { "Email has already been taken"                }
            it { displays_error message, field: 'Email', data: user.email }
          end

          describe "on empty password" do
            let(:message) { "Password can't be blank"                }
            it { displays_error message, field: 'Password', data: '' }
          end

          describe "on too short password" do
            let(:message) { "Password is too short (minimum is 6 characters)" }
            it { displays_error message, field: 'Password', data: ''          }
          end

          describe "on password - confirmation mismatch" do
            before { fill_in 'Password', with: '123456' }

            let(:message) { "Password confirmation doesn't match Password" }
            it { displays_error message, field: 'Confirmation', data: ''   }
          end
        end
      end

      describe "with valid information" do
        before do
          fill_in "Name",         with: "Example User"
          fill_in "Email",        with: "user@example.com"
          fill_in "Password",     with: "foobar"
          fill_in "Confirmation", with: "foobar"
        end

        it "creates a user" do
          expect { click_button submit }.to change(User, :count).by(1)
        end

        describe "after saving the user" do
          before { click_button submit                        }
          let(:user) { User.find_by email: 'user@example.com' }

          it { should have_title user.name                                     }
          it { should have_selector 'div.alert.alert-success', text: 'Welcome' }
        end
      end
    end
  end

  describe 'Profile page' do
    let(:user) { FactoryGirl.create :user }

    before { visit user_path(user) }

    it { should have_content user.name }
    it { should have_title   user.name }
  end
end
